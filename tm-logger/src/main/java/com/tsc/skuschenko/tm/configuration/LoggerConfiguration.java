package com.tsc.skuschenko.tm.configuration;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan("com.tsc.skuschenko.tm")
public class LoggerConfiguration {

}
