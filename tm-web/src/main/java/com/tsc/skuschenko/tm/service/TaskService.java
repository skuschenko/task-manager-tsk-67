package com.tsc.skuschenko.tm.service;

import com.tsc.skuschenko.tm.api.service.ITaskService;
import com.tsc.skuschenko.tm.exception.empty.EmptyIdException;
import com.tsc.skuschenko.tm.exception.empty.EmptyNameException;
import com.tsc.skuschenko.tm.exception.entity.task.TaskNotFoundException;
import com.tsc.skuschenko.tm.model.Task;
import com.tsc.skuschenko.tm.repository.TaskRepository;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Optional;

public class TaskService implements ITaskService {

    @NotNull
    @Autowired
    private TaskRepository taskRepository;

    @Override
    public void clearAll() {
        taskRepository.clearAll();
    }

    @Override
    @NotNull
    public Task create(@Nullable final String name) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        @NotNull final Task task =
                new Task(name);
        save(task);
        return task;
    }

    @Override
    @Nullable
    public Collection<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    @Nullable
    public Task findById(@Nullable final String id) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        return taskRepository.findTaskById(id);
    }

    @Override
    public void remove(@Nullable final Task task) {
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        taskRepository.delete(task);
    }

    @Override
    public void removeById(@Nullable final String id) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        taskRepository.removeById(id);
    }

    @Override
    public void save(@Nullable final Task task) {
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        taskRepository.save(task);
    }

}
