package com.tsc.skuschenko.tm.client;

import com.tsc.skuschenko.tm.model.Task;
import feign.Feign;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@FeignClient(value = "task")
public interface TaskRestClient {

    @NotNull
    String CREATE_ALL_METHOD = "/createAll";

    @NotNull
    String CREATE_METHOD = "/create";

    @NotNull
    String DELETE_ALL_METHOD = "/deleteAll";

    @NotNull
    String DELETE_BY_ID_METHOD = "/deleteById/{id}";

    @NotNull
    String FIND_ALL_METHOD = "/findAll";

    @NotNull
    String FIND_BY_ID_METHOD = "/findById/{id}";

    @NotNull
    String SAVE_ALL_METHOD = "/saveAll";

    @NotNull
    String SAVE_METHOD = "/save";

    @NotNull
    String URL = "http://localhost:8080/api/tasks";

    static TaskRestClient client() {
        @NotNull final FormHttpMessageConverter converter =
                new FormHttpMessageConverter();
        @NotNull final HttpMessageConverters converters =
                new HttpMessageConverters(converter);
        @NotNull final ObjectFactory<HttpMessageConverters> objectFactory =
                () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(TaskRestClient.class, URL);
    }

    @PostMapping(CREATE_METHOD)
    void create(@RequestBody @NotNull final Task task);

    @PostMapping(CREATE_ALL_METHOD)
    void createAll(@RequestBody @NotNull final Collection<Task> tasks);

    @DeleteMapping(DELETE_ALL_METHOD)
    void deleteAll(@RequestBody @NotNull final Collection<Task> tasks);

    @DeleteMapping(DELETE_BY_ID_METHOD)
    void deleteById(@PathVariable("id") @NotNull final String id);

    @GetMapping(FIND_BY_ID_METHOD)
    Task find(@PathVariable("id") @NotNull final String id);

    @GetMapping(FIND_ALL_METHOD)
    Collection<Task> findAll();

    @PutMapping(SAVE_METHOD)
    void save(@RequestBody @NotNull final Task task);

    @PutMapping(SAVE_ALL_METHOD)
    void saveAll(@RequestBody @NotNull final Collection<Task> tasks);

}
