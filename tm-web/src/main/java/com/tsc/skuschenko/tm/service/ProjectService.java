package com.tsc.skuschenko.tm.service;

import com.tsc.skuschenko.tm.api.service.IProjectService;
import com.tsc.skuschenko.tm.exception.empty.EmptyIdException;
import com.tsc.skuschenko.tm.exception.empty.EmptyNameException;
import com.tsc.skuschenko.tm.exception.entity.project.ProjectNotFoundException;
import com.tsc.skuschenko.tm.model.Project;
import com.tsc.skuschenko.tm.repository.ProjectRepository;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Optional;

public class ProjectService implements IProjectService {

    @NotNull
    @Autowired
    private ProjectRepository projectRepository;

    @Override
    public void clearAll() {
        projectRepository.clearAll();
    }

    @Override
    @NotNull
    public Project create(@Nullable final String name) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        @NotNull final Project project =
                new Project(name);
        save(project);
        return project;
    }

    @Override
    @Nullable
    public Collection<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    @Nullable
    public Project findById(@Nullable final String id) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        return projectRepository.findProjectById(id);
    }

    @Override
    public void remove(@Nullable final Project project) {
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        projectRepository.delete(project);
    }

    @Override
    public void removeById(@Nullable final String id) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        projectRepository.removeById(id);
    }

    @Override
    public void save(@Nullable final Project project) {
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        projectRepository.save(project);
    }

}
