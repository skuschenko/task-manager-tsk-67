package com.tsc.skuschenko.tm.api.service;

import com.tsc.skuschenko.tm.model.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public interface ITaskService {

    void clearAll();

    @NotNull Task create(@Nullable String name);

    @Nullable Collection<Task> findAll();

    @Nullable Task findById(@Nullable String id);

    void remove(@Nullable Task task);

    void removeById(@Nullable String id);

    void save(@NotNull Task task);

}
