package com.tsc.skuschenko.tm.listener;

import com.tsc.skuschenko.tm.event.ConsoleEvent;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor

public abstract class AbstractListener {

    @Nullable
    public abstract String arg();

    @Nullable
    public abstract String description();

    public abstract void handler(@NotNull final ConsoleEvent event);

    @Nullable
    public abstract String name();

    @Nullable
    public String[] roles() {
        return null;
    }

    protected void showOperationInfo(@NotNull final String info) {
        System.out.println("[" + info.toUpperCase() + "]");
    }

    protected void showParameterInfo(@NotNull final String info) {
        System.out.println("ENTER " + info.toUpperCase() + ":");
    }

}
